using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputBlockedController : MonoBehaviour
{
    [SerializeField] private SimpleCharacterController charController;

    public void StartCutScene()
    {
        charController.isMovementBlocked = true;
    }

    public void EndCutScene()
    {
        charController.isMovementBlocked = false;
    }
}
