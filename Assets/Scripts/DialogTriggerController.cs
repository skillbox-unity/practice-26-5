using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class DialogTriggerController : MonoBehaviour
{
    [SerializeField] private PlayableDirector dialogTimeline;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            dialogTimeline.Play();
        }
    }
}

